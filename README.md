# Glossy Ghost Theme

A _private_ theme for [Ghost](https://ghost.org) by [Camp Jefferson](https://github.com/campjefferson)

---

## Example

Used for the GlossyInc blog, which can be viewed [here](http://glossyinc.com)

## Setup

1\. Clone Repository to themes folder `/content/themes/`

```bash
git clone https://campjefferson@bitbucket.org/glossyinc/glossy-ghost-theme.git 
```

2\. Install dev dependencies

```bash
npm install
```

3\. Compile and minify sass

```bash
npm run init
```

4\. Start your Ghost application  
5\. Navigation to /ghost/settings/general/ and select **glossy** from the Theme drop down.  
6\. Save and refresh your blog and the theme should now be loaded.  
