'use strict';

var 
    gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    nano = require('gulp-cssnano'),
    rename = require('gulp-rename')
;

gulp.task('sass', function () {
    return gulp.src('./src/sass/style.scss')
        .pipe(plugins.sass().on('error', plugins.sass.logError))
        .pipe(plugins.concatCss("style.min.css"))
        .pipe(nano())
        .pipe(gulp.dest('./assets/css'))
    ;
});

gulp.task('sass:dev', function () {
    return gulp.src('./src/sass/**/*.scss')
        .pipe(plugins.sass().on('error', plugins.sass.logError))
        .pipe(plugins.concatCss("style.css"))
        .pipe(gulp.dest('./assets/css'))
    ;
});

gulp.task('js', function() {
    return gulp.src('./src/scripts/**/*.js')
        .pipe(plugins.concat('glossy.js'))
        .pipe(plugins.minify({
            ext: {
                src: ".js",
                min: ".min.js"
            }
        }))
        .pipe(gulp.dest('./assets/js'))
    ;
});

//Watch task
gulp.task('watch',function() {
    gulp.watch('./src/sass/**/*.scss', ['sass']);
});

gulp.task('default', ['sass','js']);